'use strict'
//HW8/////////////////

/*
Опишіть своїми словами що таке Document Object Model (DOM)
Яка різниця між властивостями HTML-елементів innerHTML та innerText?
Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Відповіді
1.forEach это метод, который перебирает все вложенные элементы масива на котором он вызван
и к каждому из них применяет калбэк функцию которая имеет три параметра
сам элемент, индекс элемента, и сам масив

2.В JS существует 4 способа вызвать функцию. Вызов определяет значение this или «владельца» функции.

Вызов в качестве функции.Вызов в качестве метода. Вызов в качестве конструктора.Вызов с помощью методов apply или call.
3.Поднятие — это термин, описывающий подъем переменной или функции в глобальную или функциональную области видимости.
/*

Реалізувати функцію фільтру масиву за вказаним типом даних. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
*/
const allPageParagraphs = document.querySelectorAll("p");

allPageParagraphs.forEach(element => {
    element.style.backgroundColor = "#ff0000";
});

const getElementWithId = document.querySelector("#optionsList");

console.log(getElementWithId);
console.log("parrent node of element with ID", getElementWithId.parentElement);
console.log("childNodes of element with ID", getElementWithId.childNodes);


getElementWithId.childNodes.forEach(element => {
    console.log(`it\s ${element.nodeName} with type of ${element}`);
});


const testParagraph = document.querySelector("#testParagraph ");

testParagraph.innerText = "This is a paragraph";

const getChildElementsWithClassMainHeader = document.querySelectorAll(".main-header > *");

getChildElementsWithClassMainHeader.forEach(element => {
    element.classList.add("nav-item");
    console.log(element);
});


console.log(getChildElementsWithClassMainHeader);


const getAllElementsWithClassHeaderTitle = document.querySelectorAll(".header-title");

getAllElementsWithClassHeaderTitle.forEach(element => {
    element.classList.remove("header-title")
})

console.log(getAllElementsWithClassHeaderTitle);