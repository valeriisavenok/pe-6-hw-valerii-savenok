'use strict'
//HW4/////////////////

/*
Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
Які засоби оголошення функцій ви знаєте?
Що таке hoisting, як він працює для змінних та функцій?

Відповіді
1.Экранирование используем для того чтоб показать интерпретатору что следующий символ по коду будет
// просто символом, а не частью выражения. Пример: экранирование кавычек.
// Также бывает и наоборот когда нужно указать интерпретатору что будет использоваться часть выражения,
// а не строка. Пример: добавление символа из Unicode \u{1f600}

2.В JS существует 4 способа вызвать функцию. Вызов определяет значение this или «владельца» функции.

Вызов в качестве функции.Вызов в качестве метода. Вызов в качестве конструктора.Вызов с помощью методов apply или call.
3.Поднятие — это термин, описывающий подъем переменной или функции в глобальную или функциональную области видимости.
/*

Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
*/
function createNewUser() {
	const newUser = {
		_setFirstName: ``,
		set setFirstName(value) {
			this._setFirstName = value;
		},
		get setFirstName() {
			return this._setFirstName;
		},

		_setLastName: ``,
		set setLastName(value) {
			this._setLastName = value;
		},
		get setLastName() {
			return this._setLastName;
		},

		_birthday: new Date(),
		set birthday(value) {
			this._birthday = value;
		},
		get birthday() {
			return this._birthday;
		},

		getLogin() {
			return `${this.setFirstName[0]}${this.setLastName}`.toLowerCase();
		},

		getAge() {
			const today = new Date();
			const userBirthday = Date.parse(
			  `${this.birthday.slice(6)}-${this.birthday.slice(3,5)}-${this.birthday.slice(0, 2)}`);
			const age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
			if (age < today) {
			  return `Вам ${age - 1} лет`;
			} else {
			  return `Вам ${age} лет`;
			}
		  },
		
		getPassword() {
			return `${this.setFirstName[0].toUpperCase()}${this.setLastName.toLowerCase()}${this.birthday.getFullYear()}`;
		}
	};

	newUser.setFirstName = prompt(`Enter your first name`);
	newUser.setLastName = prompt(`Enter your last name`);
	newUser.birthday = prompt("Введите дату Вашего рождения текст в формате dd.mm.yyyy");  

	console.log(newUser.getLogin());
	console.log(newUser.getAge());
	console.log(newUser.getPassword());

	return newUser;
};

console.log(createNewUser());


