
'use strict';
/*
Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
*/

function creatingList(array, target = document.body) {
	const ul = document.createElement(`ul`);

	array.forEach(el => {
		const li = document.createElement('li');
		if (Array.isArray(el)) {
			creatingList(el, li);
		}
		else {
			li.textContent = el;
		};
		ul.append(li);
	});


	target.append(ul);
};

creatingList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);