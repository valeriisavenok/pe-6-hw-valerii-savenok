//HW2
// Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
// Цыклы нужны, чтобы выполнить одинаковые действия какое-то определенное количество раз или пока не будет выполненно какое-то условие.
"use strict";

let noNumbers = true;
let num;
while (!num) {
  num = +prompt("Введите значение");
}

for (let i = 1; i <= num; i++) {
  if (i % 5 == 0) {
    console.log(i);
    noNumbers = false;
  }
}

if (noNumbers) {
  alert(`Sorry, no numbers`);
}
