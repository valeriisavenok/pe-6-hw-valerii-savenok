`use strict`;

document.addEventListener(`DOMContentLoaded`, () => {

	const btn = document.querySelector(`.theme-color`);
	const styleFile = document.querySelector(`#style-theme-color`);
	let colorFlag;

	function setThemeColor() {
		const themeColor = localStorage.getItem(`themeColor`);
		if (!themeColor || themeColor === `light`) {
			styleFile.href = `./css/style-light.css`;
			btn.textContent = `Темная тема`;
			colorFlag = `light`;
		}
		if (themeColor === `dark`) {
			styleFile.href = `./css/style-dark.css`;
			btn.textContent = `Светлая тема`;
			colorFlag = `dark`;
		}
	}
	setThemeColor();

	btn.addEventListener(`click`, btnOnCLick);

	function btnOnCLick() {
		if (colorFlag === `light`) {
			localStorage.setItem(`themeColor`, `dark`);
			setThemeColor();
		} else if (colorFlag === `dark`) {
			localStorage.setItem(`themeColor`, `light`);
			setThemeColor();
		}
	};

});
