"use strict";

/**

Задание 1.
Получить и вывести в консоль следующие элементы страницы:
По идентификатору (id): элемент с идентификатором list;
По классу — элементы с классом list-item;
По тэгу — элементы с тэгом li;
По CSS селектору (один элемент) — третий li из всего списка;
По CSS селектору (много элементов) — все доступные элементы li.
Вывести в консоль и объяснить свойства элемента:
innerText;
innerHTML;
outerHTML. */

// let listById = document.getElementById("root");
// // console.log(listById);

// let listByClass = document.getElementsByClassName("list-item");
// // console.log(listByClass);

// let listByTag = document.getElementsByTagName("li");
// // console.log(listByTag);

// let listBySelector = document.querySelector("li:nth-child(3)");
// // console.log(listBySelector);

// let listBySelectorAll = document.querySelectorAll("li");
// // console.log(listBySelectorAll);

// let myElem = document.getElementById("listId");
// // console.log(myElem.innerText);
// // console.log(myElem.innerHTML);
// // console.log(myElem.outerHTML);

// myElem.innerText = "<p>blablabla</p>";

/////////////////

// Задание 2.
// Получить элемент с классом .remove.
// Удалить его из разметки.
// Получить элемент с классом .bigger.
// Заменить ему CSS-класс .bigger на CSS-класс .active.
// Условия:
// Вторую часть задания решить в двух вариантах: в одну строку и в две
// строки.

// let getElemClassRem = document.querySelector(".remove");
// console.log(getElemClassRem);
// getElemClassRem.remove();

// let getElemClassBig = document.querySelector(".bigger");
// console.log(getElemClassBig);
// getElemClassBig.classList.remove("bigger");

// getElemClassBig.classList.add("active");
// getElemClassBig.className = getElemClassBig.className.replace(
//   "bigger",
//   "active"
// );

// getElemClassBig.classList.replace("bigger", "active");

/////////////////////////////

// На экране указан список товаров с указанием названия
// и количества на складе.
// Найти товары, которые закончились и:
// Изменить 0 на «закончился»;
// Изменить цвет текста на красный;
// Изменить жирность текста на 600.
// Требования:
// Цвет элемента изменить посредством модификации атрибута style.

// let storage = document.querySelectorAll(".store li");
// console.log(storage);

// let finishItems = document.getElementsByTagName("li");
// console.log(finishItems);

// for (let item = 0; item < finishItems.length; item++) {
//   let arr = finishItems[item].textContent.split(": ");
//   console.log(finishItems[item].textContent);
//   if (+arr[1].trim() == 0) {
//     finishItems[item].textContent = arr[0] + ": Закончился";
//     finishItems[item].style.color = "red";
//     finishItems[item].style.fontWeight = 600;
//   }
// }
// console.log(finishItems);

/////////////////////////////////////////////////

/**

Задание 4.
Получить элемент с классом .list-item.
Отобрать элемент с контентом: «Item 5».
Заменить текстовое содержимое этого элемента на ссылку, указанную в секции
«дано».
Сделать это так, чтобы новый элемент в разметке не был создан.
Затем отобрать элемент с контентом: «Item 6».
Заменить содержимое этого элемента на такую-же ссылку.
Сделать это так, чтобы в разметке был создан новый элемент.
Условия:
Обязательно использовать метод для перебора;
Объяснить разницу между типом коллекций: Array и NodeList. */

// const targetElement = "Googleit!";

// let listItems = document.querySelectorAll(".list-item");
// let a;
// for (let item = 0; item < listItems.length; item++) {
//   if (listItems[item].textContent == "Item 5") {
//     listItems[item].innerHTML = `<a href="#">${targetElement}</a>`;
//   }
//   if (listItems[item].textContent == "Item 6") {
//     listItems[item].innerHTML = "";
//     a = document.createElement("a");
//     a.innerHTML = targetElement;
//     a.href = "#";
//     listItems[item].appendChild(a);
//   }
// }

// console.log(listItems);
