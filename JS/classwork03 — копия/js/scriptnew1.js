'use strict'
// const objectA = {
//   a: 10,
//   b: true,
// }

// console.log(objectA);

// const copyOfA = objectA;

// copyOfA.c = 20;
// copyOfA.a = 100;

// console.log(objectA);

// const myCity = {
//   city: 'New York',
//   district: {podil: true},
// }

// console.log(myCity);

// myCity.city = 'Las Vegas';
// myCity = true;
// myCity.district.podil = false;

// console.log(myCity);

// delete myCity.district;
// delete myCity.city

// console.log(myCity);

// const name = 'Valera';
// const postsQty = 23;

// const userProfile = {
//   name: name,
//   postsQty: postsQty,
//   hasSignedAgreement: false,
// }

// console.log(userProfile.postsQty)

// const post = {
//   title: 'My post',
//   likesQty: 5,
//   age: 20,
// }

// const post2 = JSON.parse(JSON.stringify(post))

// post2.title = 'Object';
// post2.age = 70;

// console.log(post);
// console.log(post2);

// function myFn(a, b){
//   let c;
//   a = a + 1;
//   c = a + b;
//   return c;
// }

// let result = myFn(10, 3);
// console.log(result);

// const personOne = {
//   name: 'Bob',
//   age: 22,
// }

// function increasePersonAge(person){
//   const updatedPerson = {...person}
//   updatedPerson.age = updatedPerson.age + 1
//   return updatedPerson;
// }

// const updatedPersonOne = increasePersonAge(personOne);
// console.log(personOne.age);
// console.log(updatedPersonOne.age);


// function printMyName(){
//   console.log('Bogdan');
// }
// console.log('Start')
// setTimeout(printMyName, 3000)

// const multByFactor = (value, multiplier) => {
//     return value * multiplier
// }

// console.log(multByFactor(10, 2));
// console.log(multByFactor(5, 2));



// const newPost = (post, addedAt = Date()) => {
//     ...post,
//     addedAt,
//     return newPost
// }

// const firstPost = {
//     id: 1,
//     author: 'Bogdan',
// }

// console.log(newPost(firstPost));


// const fnWithError = () => {
//     throw new Error('Some error')
// }
// try {
//     fnWithError()
// }catch(error){
//     console.error(error)
//     console.log(error.message)
// }
// console.log('Continue...');


// function myFunc(arr, parent = document.body) {

//   arr.forEach((element) => {
//     let li = document.createElement("li");
//     li.innerHTML = element;
//     parent.appendChild(li);
//   }); 

//   let newArray = arr.map(function (item) {
//     return `<li>${item}</li>`;
//   });
//   parent.innerHTML = newArray.join("");
// }

// myFunc(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"],
//   document.querySelector(".list"));

// const someArr = [
//     "some",
//     "new",
//     "arr",
//     "here",
//     "inside",
//     "hello",
//     "world",
// ]

// function myFunc(arr, parent = document.body){
//     const ul = arr.map(element => {
//         return `<li>${element}</li>`
//     });
//     parent.innerHTML = `<ul>${ul.join('')}</ul>`
// }

// myFunc(someArr);

// const myArray = [1, true, 'Bogdan']

// const myArray2 = [1, true, 'Bogdan']

// myArray.push(4)


// console.log(myArray)
// console.log(myArray.length)
// console.log(("b" + "a" + +"a" + "a").toLowerCase());



// const myArray = [1, 2, 3]
// console.log(myArray);

// const newArray = myArray.map(el => {
//   return  (el * 3)
// })
// console.log(newArray)
// console.log(myArray)

