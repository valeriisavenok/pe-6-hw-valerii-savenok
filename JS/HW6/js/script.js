// Опишите своими словами как работает цикл forEach.
// Цикла forEach нету, есть метод forEach - который перебирает все вложенные элементы масива на котором он вызван
// и к каждому из них применяет калбэк функцию которая имеет три параметра
// сам элемент, индекс элемента, и сам масив

"use strict";

function filterBy(array, type) {
  return array.reduce((res, currentItem) => {
    if (typeof currentItem != type) {
      res.push(currentItem);
    }
    return res;
  }, []);
}

console.log(filterBy(["hello", "world", 23, "23", null], "string"));
