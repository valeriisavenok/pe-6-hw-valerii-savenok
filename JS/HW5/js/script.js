// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Экранирование используем для того чтоб показать интерпретатору что следующий символ по коду будет
// просто символом, а не частью выражения. Пример: экранирование кавычек.
// Также бывает и наоборот когда нужно указать интерпретатору что будет использоваться часть выражения,
// а не строка. Пример: добавление символа из Unicode \u{1f600}

"use strict";

class CreateNewUser {
  constructor() {
    this.firstName = prompt("Введите Ваше имя");
    this.lastName = prompt("Введите Вашу фамилию");
  }

  getLogin() {
    return `${this.firstName[0].toLowerCase()}.${this.lastName.toLowerCase()}`;
  }



  getPassword() {
    return `${this.firstName[0].toLocaleLowerCase()}${this.lastName.toLocaleLowerCase()}`;
  }
}

const newUser = new CreateNewUser();
console.log(newUser.getPassword());
