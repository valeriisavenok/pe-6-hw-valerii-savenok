`use strict`;

document.onkeydown = function (event) {
  let keycode = event.code;
  let key = document.querySelector(`[data-code = '${keycode}' ]`);

  if (key) {
    document.querySelectorAll(".btn").forEach((element) => {
      element.style.background = "black";
    });
    key.style.background = "blue";
  }
};

//Должен быть массив с картинками,функция js должна взять первый элемент массива и вставить его значение в атриббут(src) картинки на сайте.
