// По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
// Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета Нужно ввести одинаковые значения
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
document.onclick = function (event) {
  let element = event.target;
  if (element.classList.contains("icon-password")) {
    let atribut = element.getAttribute("data-pass");
    if (element.classList.contains("fa-eye")) {
      element.classList.remove("fa-eye");
      element.classList.add("fa-eye-slash");
      document.getElementById(atribut).setAttribute("type", "text");
    } else {
      document.getElementById(atribut).setAttribute("type", "password");
      element.classList.add("fa-eye");
      element.classList.remove("fa-eye-slash");
    }
  } else if (element.classList.contains("btn")) {
    if (
      document.getElementById("password1").value ==
      document.getElementById("password2").value
    ) {
      document.querySelector(".spanPass").style.display = "none";

      alert("You are welcome");
    } else {
      document.querySelector(".spanPass").style.display = "block";
    }
  }
};
