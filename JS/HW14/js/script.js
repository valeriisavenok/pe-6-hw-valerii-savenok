$(document).ready(function (event) {
  $(document).on("click", ".btn_hide", function () {
    if ($(".popular:visible").length) {
      $(".btn_hide").text("Показать");
    } else {
      $(".btn_hide").text("Скрыть");
    }

    $(".popular").slideToggle("slow");
  });

  $(document).on("click", ".nav__link", function (event) {
    let id = $(this).attr("href");

    if (id[0] == "#") {
      event.preventDefault();
      id = "." + id.slice(1, id.length);
      let top = $(id).offset().top - 65;
      $("body, html").animate({ scrollTop: top }, 1000);
    }
  });
});

$(window).scroll(function () {
  if ($(this).scrollTop()) {
    $("#toTop").fadeIn();
  } else {
    $("#toTop").fadeOut();
  }
});

$("#toTop").click(function () {
  $("html, body").animate({ scrollTop: 0 }, 1000);
});
