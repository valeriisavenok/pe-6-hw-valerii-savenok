"use strict";

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?

// Це коли ми один раз записуємо якийсь метод чи дії в прототип функції або функції конструктора и він будет автоматично копіюватися у всі інші функції які будуть створені за допомогою конструктора чи класса

// Для чого потрібно викликати super() у конструкторі класу-нащадка
// Для того шоб скопіювати якісь вже задані аргументи з минулої функції та перезаписати їх значення або просто передати ці аргументи наступній функції

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(newName) {
    return (this._name = newName);
  }
  get age() {
    return this._age;
  }
  set age(newAge) {
    return (this._age = newAge);
  }
  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    return (this._salary = newSalary);
  }
}

class Programmer extends Employee {
  constructor(lang, ...args) {
    super(...args);
    this.lang = lang;
  }
  get salary() {
    return super.salary * 3;
  }
}

const employee = new Employee("Sasha", 20, 5000);
const programmer = new Programmer("PHP", "Bob", 25, 5000);
const superProgrammer = new Programmer("PHP", "SuperPr", 35, 5000);
const adminProgrammer = new Programmer("PHP", "Admin", 50, 5000);
