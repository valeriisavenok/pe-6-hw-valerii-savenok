"use strict";

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Ajax це технологія яка дозволяє робити обмін данними з сервером не перезавантажуючи сторінку

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

{
  /* <div class="lds-ring"><div></div><div></div><div></div><div></div></div> */
}

const container = document.querySelector(".container");
fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((res) => res.json())
  .then((data) => {
    const wrapper = document.createElement("div");
    wrapper.classList.add("wrapper");
    let charactersArr = [];
    data.forEach(({ episodeId, name, openingCrawl, characters }, index) => {
      const card = document.createElement("div");
      const ul = document.createElement("ul");
      ul.innerHTML =
        '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>';
      ul.classList.add("story", `story_${index}`);
      card.append(ul);
      card.classList.add("card");
      card.insertAdjacentHTML(
        "afterbegin",
        `
        <h2 class="eposideId">${episodeId}</h2>
        <h3 class="movieName">${name}</h3>
        <p class="description">
          ${openingCrawl}
        </p>
      `
      );
      wrapper.append(card);
      charactersArr.push(characters);
    });
    container.append(wrapper);
    return charactersArr;
  })
  .then((data) => {
    data.forEach((el, index) => {
      return Promise.all(
        el.map((el) =>
          fetch(el)
            .then((res) => res.json())
            .then(({ name }) => `<li>${name}</li>`)
        )
      ).then((data) => {
        const container = document.querySelector(`.story_${index}`);
        container.innerHTML = data.join("");
      });
    });
  });
